#coding: utf-8
'''
must remind position
'''

import rospy
import sqlite3
from clover import srv
from std_srvs.srv import Trigger
from sqlite3 import Error

def create_connection(path): # путь к БД
	connection = None
	try:
		connection = sqlite3.connect(path) # если БД существует, то мы к ней подключаемся, если нет, то по указаному пути будет создана новая БД
		print("Connection to SQLite DB successful") # вывод успешного подключения к БД
		cursor.execute("""CREATE TABLE position 
					  (x double, y double, z double)
					  INSERT INTO position (telemetry.x, telemetry.y, telemetry.z)
					  """) # создание таблицы и запись в неё позиции коптера
	except Error as e: # перехват любого исключения, если не удастся установить соединение 
		print(f"The error '{e}' occurred") # сообщение об ошибке
	return connection

def back_to_home():

	rospy.init_node('flight')
	get_telemetry = rospy.ServiceProxy('get_telemetry', srv.GetTelemetry)
	navigate = rospy.ServiceProxy('navigate', srv.Navigate)
	navigate_global = rospy.ServiceProxy('navigate_global', srv.NavigateGlobal)
	set_position = rospy.ServiceProxy('set_position', srv.SetPosition)
	set_velocity = rospy.ServiceProxy('set_velocity', srv.SetVelocity)
	set_attitude = rospy.ServiceProxy('set_attitude', srv.SetAttitude)
	set_rates = rospy.ServiceProxy('set_rates', srv.SetRates)
	land = rospy.ServiceProxy('land', Trigger)


	global position = get_telemetry() # запомнить позицию
	print(telemetry.x, telemetry.y, telemetry.z)

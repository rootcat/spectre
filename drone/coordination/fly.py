#coding: utf-8
"""
The program is responsible for the flight of one or more drones to a certain point.
In order for the drone to fly where you need to specify the profile and coordinates x y z.
profile:
sport - during the flight , the speed gradually increases
home - designed for indoor flights. the drone is oriented by aruco_map
normal - default mode
outside - designed for outdoor flights. Flights are made via GPS
acro - It flies on acro that is, without pitch restrictions
"""

import rospy
from clover import srv
from std_srvs.srv import Trigger


def fly(x=0, y=0 , z=0, profile='normal'):
    rospy.init_node('flight')
    navigate = rospy.ServiceProxy('navigate', srv.Navigate)
    land = rospy.ServiceProxy('land', Trigger)

    navigate(
        x=0, y=0, z=0, frame_id='body', auto_arm=True
    ) # turns on the engines

    match profile:
        case 'sport':
            print('activate sport_progile...')
            t = int(input('Укажите необходмое время'))
            if t == 1 or t <= 0:
                t = 5
            vx = x/(t-1)
            vy = y/(t-1)
            vz = z/(t-1)
            set_velocity(
                vx=vx, vy=vy, vz=vz, frame_id='body'
            )
        case 'home':
            print('activate home_profile...')
            if z >= 4:
                z = 3
            navigate(
                x=x, y=y, z=z, frame_id='aruco_map'
            )

        case 'normal' :
            print('activate normal_profile...')
            navigate_global = rospy.ServiceProxy('navigate_global', srv.NavigateGlobal)
            navigate(
                x=x, y=y, z=z, frame_id='body'
            )

        case 'outside':
            print('activate outside_profile...')
            navigate_global(
                lat=x, lon=y, z=z, speed=5, frame_id='body'
            )

        case 'acro':
            print('activate acro_profile...')

#coding: utf-8
from uuid import uuid4
import yaml
import os
import sys
from colorama import Fore, Back, Style, init
import signal
init()

try:
    from drone import Drone
except ModuleNotFoundError:
    print(Fore.RED + "\n[Error 2] Module not found! Сheck the integrity of the program!\n")
    sys.exit(0)
# Just an example, but it works


def sigint_handler(signal, frame):
    print("\nExiting...")
    os._exit(os.EX_OK)

def load_config(filename):
    with open(filename) as f:
        config = yaml.safe_load(f)

    if not config['Entity']['id']:
        config['Entity']['id'] = str(uuid4())
        with open(filename, "w") as f:
            yaml.dump(config, f, default_flow_style=False)

    if config['Entity']['type'] != 'drone':
        config['Entity']['type'] = 'drone'
        with open(filename, "w") as f:
            yaml.dump(config, f, default_flow_style=False)
    return config


if __name__ == '__main__':
    signal.signal(signal.SIGINT, sigint_handler)
    drone_config = load_config("../core/netconfig.yaml")
    print("Drone config: ", drone_config)

    drone = Drone('127.0.0.1', 8886, str(uuid4()), message_type="ping")

    while True:
        message = input("\nSend message to drone (example: \"command, argument1, argument2\"): ").replace(' ', '').split(',')
        drone.send(command=message[0], args=message[1:])

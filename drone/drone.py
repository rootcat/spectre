#coding: utf-8
import os
import sys
import json
from uuid import uuid4
from socket import *
from threading import Thread
import queue
import sys
from colorama import Fore, Back, Style, init
init()

sys.path.append(os.path.dirname(os.path.realpath('.')))
try:
    from core import BaseDrone
    # from .drone import load_config
except ModuleNotFoundError:
    print(Fore.RED + "\n[Error 2] Module not found! Сheck the integrity of the program!\n")
    sys.exit(0)


class Drone(BaseDrone):
    def __init__(self, ip: str, port: int, entity_id: str, message_type: str):
        super().__init__(ip, port, entity_id, message_type)

        self.s = socket(AF_INET, SOCK_STREAM)
        self.s.connect((ip, port))
        self.run()

    def accept(self):
        super().accept(self.s)

    def send(self, command: str, args: list):
        super().send(self.s, command, args)

    def command_result_handler(self):  # Here we send the result from commands
        while True:
            result = self.result_queue.get()
            if result:
                self.s.send(bytes(
                        result,
                        encoding="utf-8"
                ))

    def get_params(self):
        params = load_config(file)
        return params

    def run(self):
        command_result_handler = Thread(target=self.command_result_handler)
        accept = Thread(target=self.accept)

        command_result_handler.start()
        accept.start()

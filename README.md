# Spectre-network

<p align="left"> <a href="https://www.linux.org/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a> <a href="https://www.python.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> </p>

#### **What is it?**
- How i can use it?*

#### **Collaborate**
- License
- Contact

#### **Getting started**
- Packages
- Install
- Install from pip

#### **Сonfiguration**
- Config
- Code modules

## 🔥 What is it?
![](https://i.imgur.com/H2oYOCn.png)

**Pass**
#### How i can use it?
- pass
- pass

## 🔗 Collaborate

### Licese
Spectre is published under the **free [GPLv3](https://gitlab.com/rootcat/spectre/-/blob/main/LICENSE) license,** *you can modify* the program for yourself, and *freely distribute* it, without violate the gnu license of course!

### Contact

You can write to **me** (rootcat) here:  
```
rootcat@riseup.net
```

## 🌺 Getting started

🐧 **Spectre works only on unix and unix-like systems! You can use GNU/Linux but supposedly should work on FreeBSD and MACos (not tested)**

### Packages

*Note: please сheck the spelling of the package on your distribution page.*

#### For Arch (btw) based distro:
```
sudo pacman -S beep
```
#### For Debian based:
```
sudo apt-get install beep
```

### Installation

Clone repo 🦊  
```
git clone https://gitlab.com/rootcat/spectre
```

Install pip libs 🐍  
```
pip install requests
```

### Now you can boot spectre with
```
python main.py
```
## 🦄 pass

'''
easy lib for pynput
'''

import signal
import binds
from pynput import keyboard


def shutdown_handler(*args):  # ctrl+c react
    # Do some clean up here.
    raise SystemExit('\nExiting...')


signal.signal(signal.SIGINT, shutdown_handler)


def get_binds():
    mod_key_binds = {}

    for bind in binds.binds:
        mod_key_binds[binds.what_mod_key + '+' + bind] = binds.binds.get(bind)

    return mod_key_binds


def listen():
    with keyboard.GlobalHotKeys(get_binds()) as hotkeys_listener:
        hotkeys_listener.join()

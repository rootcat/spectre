# Entity config


# Entity type determines is it a drone or a server.
# Possible values: "drone", "server", "admin-panel"
ENTITY_TYPE = None

# Entity id is a unique id, that has this entity in the infrastructure
ENTITY_ID = None

# Entity network is a dictionary, that determines network settings (ssid and password, for example)
ENTITY_NETWORK = None

'''
             Here is a config for Spectre's client-side
                            Welcome!
'''

# Here are the binds and modules (functions in tools module) that will be executed
# binds.keys() are the key bindings, and binds.values() are the modules to execute
# default: {"1": module1, "2": module2, "3": module3}

binds = {

"1": def.one,
"2": def.two,
"3": def.tree

}

# here is a list of modules
#
#               telemetry
# telemetry.battery - get battery charge
# telemetry.health - check drone condition
#
#               controle
# lockdown.off - turn off the drone
# land.down - to land
# flip.doflip - do flip :/
#
#                other
# alias.cl - clear screen
# shot.saveshot - take shot from camera

# mod key
# mod_key is used in all key bindings
# is mod_key in use?
# default: False
mod_key = False
# you can bind mod_key as "CTRL" or "ALT" etc.
# default: "<ctrl>"
what_mod_key = "<ctrl>"


# directory for photo saving
# default: "~/photo_from_spectre"
shot_dir = "~/photo_from_spectre"


# push notifications?
# default: True
push = True


# bios beep?
# emits sound with a hardware speaker
# default: True
beep = False

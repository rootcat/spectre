"""
Please, watch this YouTube video https://www.youtube.com/watch?v=HpL6ymFEuu4
It's really important to understand what views, models and services are.
"""

import uvicorn
from fastapi import FastAPI

import views

app = FastAPI()


app.include_router(views.router)

# This function is used as a terrible way to run uvicorn server


def run_server():
    uvicorn.run(app, host="127.0.0.1", port=8000)

"""

"""


from pydantic import BaseModel
from typing import List, Optional


class Message(BaseModel):
    """Basic message in Spectre infrastructure"""
    hash: str
    sender: List[str] = []
    recipients: List[str] = []
    hands: List[int] = []
    commands: List[str] = []


class ControlServerMessage(Message, BaseModel):
    """ A basic Message with settings attribute.
        It is used by control server to send some setting and
        configuration to the drone.

        Possible values for settings: ...
    """
    settings: List[str] = []

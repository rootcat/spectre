"""
Please, watch this YouTube video https://www.youtube.com/watch?v=HpL6ymFEuu4
It's really important to understand what views, models and services are.
"""


import ruamel.yaml
import os


# Import config from network_config.yaml
# os.path.abspath('..') + '...' - This is a hack to fix the bug, when the script can't find module,
# that is in the directory one level higher. Maybe i can fix it without that ugly code, or maybe not.
with open(os.path.abspath('..') + "/spectre_network/api/network_config.yaml", "r") as config:
    network_config = ruamel.yaml.safe_load(config)
    print("SERVICES\n", network_config)


def get_entity_information():
    """Returns basic entity information like id, type etc."""
    return network_config["Entity"]

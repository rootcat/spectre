"""
Please, watch this YouTube video https://www.youtube.com/watch?v=HpL6ymFEuu4
It's really important to understand what views, models and services are.
"""


from fastapi import APIRouter
from models import Message
from services import get_entity_information

router = APIRouter()


@router.get("/api/entity")
async def view_entity_info():
    return get_entity_information()

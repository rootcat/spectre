"""Main network file of controle_server"""

from uuid import uuid4
from socket import *
import threading
import random
import json
import ruamel.yaml
import os


class ControlServer:
    def __init__(self, ip: int, port: str, entity_id: str, max_conn: int = 5):
        self.entity_type = "control_server"

        self.ip = ip
        self.port = port
        self.entity_id = entity_id
        self.run(max_conn)

    def connect(self, max_conn):
        s = socket(AF_INET, SOCK_STREAM)
        s.bind((self.ip, self.port))
        s.listen(max_conn)

        client, addr = s.accept()
        while True:
            result = client.recv(1024)

            message = json.loads(result)
            print("Message from drone: {}".format(message))

            client.sendall(
                input("Send command from server to dron: ").encode()
            )

    def switch(self, run):
        dict = {
            "land": print("Landing..."),
            "up": print("Lets'go..."),
            "somethig": "pass"
        }
        return dict.get('Invalid message')

    def run(self, count_con):
        self.connect(count_con)
        #self.switch(run="up")

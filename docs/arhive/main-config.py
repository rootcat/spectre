'''
             Here is a config for Specre
                      Welcome!
'''



# profiles
# use "home" profile for minimal movement
# default: True
home_profile = True
# maximum height (in meters!)
# default: "2"
house_height = "2"


#                     auto-lockdown
# shake
# detect shaking and auto switch off spectre
# if lockdown=True may be problems with flip and etc.
# default: True
shake = True
# shake check timeout
# default: 0.3
time_coup = 0.3

# auto-detect coup
# detect coup and auto switch off spectre
# default: True
coup = True
# coup check timeout
# default: 0.3
time_coup = 0.3


# take off from fall
# you can drop drone, and it will take off from fall
# default: False
flyfall = False


# battery check timeout
# default: 20
time_battery = 20
# response threshold  # обзовите как нибудь по другому
# default: 20
alert_battery = 20 # %


# Colors
# Example: red_scheme = color_schemes['red']
color_schemes = {
    'red': [255, 0, 0],
    'green': [0, 100, 0],
    'blue': [0, 0, 255],
    'white': [255, 255, 255],
    'orange': [231, 138, 3],
}

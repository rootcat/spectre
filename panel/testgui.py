#coding: utf-8
# -- demo.py --
import pytermgui as ptg

# with ptg.WindowManager() as manager:
#     window = ptg.Window(
#         "[wm-title]Fuck GUI!",
#         "",
#         ["YES", lambda *_: manager.exit()],
#     )
#
#     manager.add(window)
#     manager.run()

PTG_CONFIG = """\
config:
    Window:
        styles:
            border: &border-style "[60]{item}"
            corner: *border-style

    Button:
        styles:
            label: "[@235 108 bold]{item}"

    Splitter:
        chars:
            separator: "   "

markup:
    title: 210 bold
    body: 245 italic
"""


with ptg.WindowManager() as manager:
    # Initialize a loader, and load our config
    loader = ptg.YamlLoader()
    loader.load(PTG_CONFIG)

    # Create a test window
    manager.add(
        ptg.Window()
        + "[title]This is our title"
        + ""
        + {"[body]First key": ["First button"]}
        + {"[body]Second key": ["Second button"]}
        + {"[body]Third key": ["Third button", lambda *_: manager.exit()]}
        + ""
        + ["Exit", lambda *_: manager.exit()]
    )

    manager.run()

input()

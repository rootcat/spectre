#coding: utf-8
from uuid import uuid4
import yaml
import os
import sys
from colorama import Fore, Back, Style, init
init()

try:
    from panel import Panel
except ModuleNotFoundError:
    print(Fore.RED + "\n[Error 2] Module not found! Сheck the integrity of the program!\n")
    sys.exit(0)


def load_config(filename):
    with open(filename) as f:
        config = yaml.safe_load(f)

    if not config['Entity']['id']:
        config['Entity']['id'] = str(uuid4())
        with open(filename, "w") as f:
            yaml.dump(config, f, default_flow_style=False)

    if config['Entity']['type'] != 'panel':
        config['Entity']['type'] = 'panel'
        with open(filename, "w") as f:
            yaml.dump(config, f, default_flow_style=False)
    return config


if __name__ == '__main__':
    main_drone_config = load_config("../core/netconfig.yaml")
    # print("Drone config: ", main_drone_config)

    panel = Panel('127.0.0.1', 8886, str(uuid4()), message_type="ping")

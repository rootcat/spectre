#coding: utf-8
from uuid import uuid4
from socket import *
from threading import Thread
import os
import sys
from colorama import Fore, Back, Style, init
init()

sys.path.append(os.path.dirname(os.path.realpath('.')))
try:
    from core import BaseEntity
except ModuleNotFoundError:
    print(Fore.RED + "\n[Error 2] Module not found! Сheck the integrity of the program!\n")
    sys.exit(0)


class Panel(BaseEntity):

    def __init__(self, ip: str, port: int, entity_id: str, message_type: str):
        super().__init__(ip, port, entity_id, message_type)

        self.entity_type = "panel"

        self.s = socket(AF_INET, SOCK_STREAM)

        while True:
            try:
                self.s.connect((ip, port))
            except ConnectionRefusedError:
                print(Fore.RED + "\n[Error 3] Server not found!\n" + Style.RESET_ALL + "Reloading...")
                continue
            break
        self.run()

    def accept(self):

        while True:
            result = self.s.recv(1024).decode("utf-8")
            print("Message from server: {}".format(result))

            try:
                handler = Thread(target=self.handler, args=[result])
            except AttributeError:
                print(Fore.RED + "\n[Error 3] Server not found!\n" + Style.RESET_ALL + "Reloading...")
                continue
            handler.start()

    def send(self):
        while True:
            message = bytes(
                self.create_message(input("Send command from panel: ")),
                encoding="utf-8"
            )
            self.s.send(message)

    def run(self):
        accept = Thread(target=self.accept)
        send = Thread(target=self.send)
        accept.start()
        send.start()
        #self.switch(run="up")

    def beep(self, mode):  # default beep

        if mode == "default" and config.beep:
            for i in range(5):
                time.sleep(1)
                os.system("beep")

        elif mode == "alert" and  config.beep: # alert beep
            for i in range(20):
                os.system("beep")

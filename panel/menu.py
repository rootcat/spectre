#coding: utf-8
import os
import sys
from pynput import keyboard
from colorama import Fore, Back, Style, init
init()

sys.path.insert(1, os.path.abspath('.'))


class Menu:

    def listen(self):
        def on_press(pressed_key):
            global key
            key = pressed_key
            return False

        with keyboard.Listener(on_press=on_press) as listener:
            listener.join()


    def logo(self):
        os.system("clear")
        os.system("cat modules/art.txt | lolcat -F --seed=18")


    def menu(self):

        print(Fore.BLUE + "\nWelcome! This is panel menu of Spectre project!" + Fore.BLUE + "\n\n[1] help \n[2] pass \n[0] exit")
        self.listen()


        if key.char == "1":
            print("menu1")
        elif key == "g":
            print("menu2")
        else:
            print("error")


if __name__ == "__main__":
    m = Menu()
    m.logo()
    m.menu()

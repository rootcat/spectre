#coding: utf-8
from uuid import uuid4
import json
import time
import sys
from sys import platform
import os
import math
# from sensor_msgs.msg import Image
# from cv_bridge import CvBridge
# import rospy
import psutil
import queue
# import config
from colorama import Fore, Back, Style, init
from threading import Thread
from socket import SocketType
init()

sys.path.append(os.path.dirname(os.path.realpath('..')))
try:
    from core.utils import *
    from gui.main import GUIWindow
except ModuleNotFoundError:
    print(Fore.RED + "\n[Error 2] Module not found! Сheck the integrity of the program!\n")
    sys.exit(0)

# from modules import alias
# from clover import srv
# from std_srvs.srv import Trigger
# bridge = CvBridge()


class BaseEntity:
    def __init__(self, ip: str="127.0.0.1", port: str="8888", entity_id: str="0", message_type: str="send"):
        self.ip = ip
        self.port = port
        self.entity_id = entity_id
        self.message_type = message_type
        self.entity_type = "drone"

        self.commands = {
            ('drone', 'ping'): self.ping,
            ('main_drone', 'ping'): self.ping,
            ('panel', 'ping'): self.ping
        }

        self.config_commands = {
            "gui": self.start_gui
        }

        self.drones_ip = []

        self.result_queue = queue.Queue()

    def send(self, conn_socket: SocketType, command: str="", args: list=[]):
        if command in self.config_commands:
            self.config_commands[command](*args)
        else:
            message = self.create_message(command, args)
            conn_socket.sendall(bytes(
                message,
                encoding="utf-8"
            ))

    def accept(self, conn_socket: SocketType):
        while True:
            message = conn_socket.recv(1024).decode("utf-8")
            if not message:
                break
            message = json.loads(message)
            command = message['command']
            print("\nReceived message: {}".format(message))

            command_handler = Thread(
                target=lambda q, command: q.put(self.command_execution_handler(command)),
                args=[self.result_queue, command]
            )
            command_handler.start()

    def create_message(self, command: str="", args: list=[]):
        message = {
                "sender": self.entity_type,
                "recipient": "recipient_name",
                "entity_id": self.entity_id,
                "message_id": str(uuid4()),
                "message_type": self.message_type,
                "command": {'command': command, 'args': args}
        }
        message = json.dumps(message)
        return message

    def command_execution_handler(self, command: dict={}):
        #print(f"\nProcessing command: {command}")
        result = self.commands.get((self.entity_type, command['command']))
        if result:
            result = result(*command['args'])

        return result

    def check_requirements(self):
        if platform != "linux":
            input("\nNot for windows, run only on GNU/Linux!\n")
            input()
            sys.exit()
        elif os.geteuid() != 0:
            print("\nNeed run as root!\n")
            sys.exit()

    def ping(self):
        return self.create_message("pong", [])

    def cl(self):  # clear screen
        os.system("clear")

    def start_gui(self):
        GUIWindow(self)


class BaseDrone(BaseEntity):
    def __init__(self, ip, port, entity_id, message_type):
        super().__init__(ip, port, entity_id, message_type)
        self.commands.update({
            ('drone', 'led'): self.set_led_color,
            ('main_drone', 'led'): self.set_led_color
        })
        self.PI_2 = math.pi / 2

    def flip(self):
        start = get_telemetry()  # memorize starting position

        set_rates(thrust=1)  # bump up
        rospy.sleep(0.2)

        set_rates(pitch_rate=30, thrust=0.2)  # pitch flip
        # set_rates(roll_rate=30, thrust=0.2)  # roll flip

        while True:
            telemetry = get_telemetry()
            flipped = abs(telemetry.pitch) > self.PI_2 or abs(telemetry.roll) > self.PI_2
            if flipped:
                break

        rospy.loginfo('finish flip')
        set_position(x=start.x, y=start.y, z=start.z, yaw=start.yaw)  # finish flip

        print(navigate(z=2, speed=1, frame_id='body', auto_arm=True))  # take off
        rospy.sleep(10)
        rospy.loginfo('flip')

    def shot():
        rospy.init_node('computer_vision_sample')
        img = bridge.imgmsg_to_cv2(rospy.wait_for_message('main_camera/image_raw', Image), 'bgr8')

    def set_led_color(self, *colors):  # Change drone's led color
        return self.create_message(f"Led color was set to {colors}", [])

    def lockdown():  # turn off drone
        from mavros_msgs.srv import CommandBool
        arming = rospy.ServiceProxy('mavros/cmd/arming', CommandBool)
        arming(False)

    def battery_state_handler():  # auto-check battery
        battery = psutil.sensors_battery()

        while True:
            time.sleep(config.time_battery)

            if battery.percent < 20 and config.push:
                print("Low battery: ", battery.percent)
                alias.do_beep()
                battery_state_notify(str(battery.percent))

                if config.beep:
                    alias.do_beep()

    def health():  # check drone
        pass

    def shake():  # check shake
        get_telemetry = rospy.ServiceProxy('get_telemetry', srv.GetTelemetry)

        for i in range(0,10):
            yaw = get_telemetry().yaw
            pitch = get_telemetry().pitch
            roll = get_telemetry().roll
            rospy.sleep(1)
            iyaw = get_telemetry().lat
            ipitch = get_telemetry().lat
            iroll = get_telemetry().lat
            if (abs(iyaw - yaw) > 10) or (abs(ipitch - pitch) > 10) or (abs(roll - iroll) > 10):
                waring = max(abs(iyaw - yaw)), (abs(ipitch - pitch)), (abs(roll - iroll))
                i += 10
            rospy.sleep(2)

        if waring >= 30:
            print('Red danger tarbulation')
            # Led is red
            # land()
            rospy.sleep(5)

        elif waring >= 20:
            print('Yellow danger tarbulation')
            # Led is yellow
        else:
            print('Not tarbulations')
            # Led is green

    def coup():  # check coup
        while True:
            time.sleep(config.time_coup)

            self.PI_2 = math.pi / 2
            telem = get_telemetry()
            flipped = abs(telem.pitch) > self.PI_2 or abs(telem.roll) > self.PI_2
            print(flipped) # не знаю какой тут будет вывод

            # предположим что вывод будет такой:
            # up - кверху брюхом
            # down - нормальное положение


            if flipped == 'up':
                alias.lockdown()  # bye bye
                print('LOCKDOWN! Something happened with the drone!')

                if config.push:
                    os.system('''notify-send 'LOCKDOWN!' 'Something happened with the drone!' --icon=dialog-information''')

                if config.beep:
                    alias.alert_beep()

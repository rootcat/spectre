#coding: utf-8
from socket import *
from threading import Thread
from uuid import uuid4
import os
import json
import sys
from colorama import Fore, Back, Style, init
init()

sys.path.append(os.path.dirname(os.path.realpath('.')))
try:
    from core import BaseDrone
except ModuleNotFoundError:
    print(Fore.RED + "\n[Error 2] Module not found! Сheck the integrity of the program!\n")
    sys.exit(0)


class MainDrone(BaseDrone):
    def __init__(self, ip: str, port: int, entity_id: str, message_type: str, max_conn: int = 5):
        super().__init__(ip, port, entity_id, message_type)

        self.entity_type = "main_drone"

        '''Network initialization'''
        self.s = socket(AF_INET, SOCK_STREAM)
        self.s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

        while True:
            try:
                self.s.bind((ip, port))
            except OSError:
                input(Fore.RED + "\n[Error 1] Address already in use!\n" + Style.RESET_ALL + "Press [enter] to reload...")
                continue
            break

        self.s.listen(max_conn)
        self.client, self.addr = self.s.accept()
        self.run()

    def accept(self):
        super().accept(self.client)

    def send(self, command: str, args: list, client=None):
        if not client: client = self.client
        super().send(client, command, args)

    def send_everyone(self, command: str="", args: list=[]):
        for i in self.drones_ip:
            self.send(command, args, i)

    def command_result_handler(self):
        while True:
            result = self.result_queue.get()
            if result:
                self.client.sendall(bytes(
                        result,
                        encoding="utf-8"
                ))

    def run(self):
        command_result_handler = Thread(target=self.command_result_handler)
        accept = Thread(target=self.accept)

        command_result_handler.start()
        accept.start()

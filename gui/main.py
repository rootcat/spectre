#coding: utf-8
from tkinter import *


class GUIWindow():

    def __init__(self, obj):
        self.obj = obj
    
        self.root = Tk()
        self.root.title("")
        self.width = self.root.winfo_screenwidth()
        self.height = self.root.winfo_screenheight()
        self.root.geometry(f'{self.width}x{self.height}')

        self.init_gui()

    def init_gui(self):
        width = self.width
        height = self.height
    
        messages = Frame(self.root, bg="#555577", width=width/2, height=height/2)
        messages.pack(expand=1, anchor=NW)
        messages.propagate(0)
        
        from_label = Label(messages, text="from (id):", bg="#aaaaaa", font=("Calibri 14")).place(width=width/8, height=height/32, x=width/16, y=height/32)
        to_label = Label(messages, text="to (id):", bg="#aaaaaa", font=("Calibri 14")).place(width=width/8, height=height/32, x=width/16*5, y=height/32)
        message_label = Label(messages, text="message:", bg="#aaaaaa", font=("Calibri 14")).place(width=width/8*3, height=height/32, x=width/16, y=height/16*3)
        self.from_input = Entry(messages, font=("Calibri 14"))
        self.to_input = Entry(messages, font=("Calibri 14"))
        self.message_input = Text(messages, font=("Calibri 14"))
        self.from_input.place(width=width/8, height=height/32, x=width/16, y=height/32*3)
        self.to_input.place(width=width/8, height=height/32, x=width/16*5, y=height/32*3)
        self.message_input.place(width=width/8*3, height=height/32*5, x=width/16, y=height/4)
        send_button = Button(messages, text="Send", bg="#44aa44", command=self.send_gui).place(width=width/8, height=height/32, x=width/16*3, y=height/16*7)
        
        self.run()

    def send_gui(self):
        from_ = self.from_input.get()
        to_ = self.to_input.get()
        message = self.message_input.get("1.0", "end")
        
        self.obj.one_send(from_, to_, message)

    def run(self):
        self.root.mainloop()
